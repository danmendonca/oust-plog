\documentclass[a4paper]{article}

\usepackage{fontspec}
\usepackage[portuguese]{babel}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{listings}
\usepackage{url}


\begin{document}

\setlength{\textwidth}{16cm}
\setlength{\textheight}{22cm}


% ============================== %
% ============ CAPA ============ %
% ============================== %

\title{
    \Huge\textbf{Oust - Uma implementação em Prolog}
    \linebreak\linebreak\linebreak
    \Large{\textbf{Relatório Intercalar}}
    \linebreak\linebreak
    \includegraphics[height=6cm, width=7cm]{FIGS/feup.pdf}
    \linebreak \linebreak
    \Large{Mestrado Integrado em Engenharia Informática e Computação}
    \linebreak \linebreak
    \Large{Programação em Lógica}
    \linebreak
}

\author{
    \textbf{Grupo 39}
    \\
    Daniel Mendonça - ei12167
    \\
    Fernando Moreira - ei06120
    \\ \linebreak\linebreak \\ \\
    Faculdade de Engenharia da Universidade do Porto
    \\
    Rua Roberto Frias, s\/n, 4200-465 Porto, Portugal
    \linebreak\linebreak\linebreak\linebreak\linebreak\vspace{1cm}
}
\maketitle
\thispagestyle{empty}

\newpage


% ================================ %
% ============ RESUMO ============ %
% ================================ %

\section*{Resumo}
Neste relatório os autores descrevem o trabalho preliminar, executado até à data, da implementação do jogo de tabuleiro \textit{Oust} com recurso à linguagem de programação \textit{Prolog}.


% ==================================== %
% ============ INTRODUÇÃO ============ %
% ==================================== %

\section{Introdução}
O objetivo deste trabalho é fundamentalmente a aplicação prática dos conhecimentos de Programação em Lógica obtidos na unidade curricular homónima.

Os autores pretendem portanto ganhar sensibilidade para a solução de problemas utilizando uma linguagem de programação orientada ao paradigma lógico, e perceber as limitações fundamentais do modelo prático (interpretador e ambiente de execução) face ao modelo teórico.


% ===================================== %
% ============ O JOGO OUST ============ %
% ===================================== %

\section{O Jogo Oust}
O \textit{Oust} é um jogo de tabuleiro criado em 2007 por Mark Steere. É um jogo a turnos para dois adversários onde o objetivo é capturar todas as peças do oponente. No \textit{Oust} não acontecem empates, portanto, no final, existirão sempre um vencedor e um vencido. A imagem seguinte representa um tabuleiro de jogo completamente vazio.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.4]{FIGS/oust_tabuleiro.png}
\caption{Tabuleiro do \textit{Oust}}
\label{fig:oust_tabuleiro}
\end{center}
\end{figure}

O jogo começa com um tabuleiro completamente vazio (ver figura \ref{fig:oust_tabuleiro}), onde os adversários irão jogar alternadamente até ao fim, começando pelo jogador com as peças pretas.
No seu turno, um jogador tem a possibilidade de fazer nenhuma, uma ou mais jogadas, e em cada uma delas poderá colocar apenas uma das suas peças no tabuleiro. Se uma jogada resultar na colocação de uma peça, esta deverá ser colocada numa interseção entre uma linha vertical e horizontal que esteja desocupada.

Diz-se ainda que duas peça são adjacentes se entre si não existir nenhuma peça um uma posição (interseção) vazia. Adjacência diagonal não é considerada. 


% ================================================================= %
% ============ SUB: TIPOS DE JOGADA E MUNDAÇA DE TURNO ============ %
% ================================================================= %

\subsection{Tipos de jogadas e mudança de turno}
\label{sec:jogadas-e-turnos}
Existem apenas dois tipos de jogadas: \textit{Captura} e \textit{Não-Captura}.

Uma jogada de \textit{Não-Captura} resulta na adjacência da peça colocada a peças do adversário ou na inexistência de qualquer adjacência, situação em que a peça ficará isolada. Na figura \ref{fig:oust_nao-captura} os pontos verdes denotam as jogadas de \textit{Não-Captura} possíveis para o jogador com as peças pretas.

Este tipo de jogada determina a mudança de turno.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.3]{FIGS/oust_nao-captura.png}
\caption{Possibilidades de jogada de \textit{Não-Captura}}
\label{fig:oust_nao-captura}
\end{center}
\end{figure}

Por outro lado, uma jogada de \textit{Captura} resulta sempre na criação de um grupo de peças. Um grupo de peças é um conjunto com uma ou mais peças adjacentes.
Um jogador apenas pode fazer uma jogada de \textit{Captura} se o novo grupo estiver ou ficar adjacente a um ou mais grupos de peças do adversário e a quantidade de peças nesse grupo também for superior à quantidade de peças dentro dos grupos adjacentes do adversário. Dois grupos são adjacentes quando pelo menos duas peças, uma de cada grupo, também forem adjacentes.

Este tipo de jogada resulta na remoção de todas as peças dos grupos adjacentes do adversário. Um jogador pode fazer as jogadas de \textit{Captura} que pretender num só turno, desde que estas sejam consecutivas.

Na figura \ref{fig:oust_tipos_captura} são apresentadas quatro situações onde o jogador com as peças pretas poderá fazer uma jogada de \textit{Captura}

\begin{figure}[h!]
\centering
\subfloat[Captura de três peças brancas]{
    \includegraphics[scale=0.25]{FIGS/oust_captura_tres.png}
    \label{fig:oust_captura_tres}
}
\quad\quad\quad
\subfloat[Captura de quatro peças brancas]{
    \includegraphics[scale=0.25]{FIGS/oust_captura_quatro.png}
    \label{fig:oust_captura_quatro}
}
\quad\quad\quad
\subfloat[Captura de um grupo de peças brancas]{
    \includegraphics[scale=0.25]{FIGS/oust_captura_grupo.png}
    \label{fig:oust_captura_grupo}
}
\quad\quad\quad
\subfloat[Captura de dois grupos de peças brancas]{
    \includegraphics[scale=0.25]{FIGS/oust_captura_2grupo.png}
    \label{fig:oust_captura_2grupo}
}
\caption{Vários cenários para a jogada de \textit{Captura}}
\label{fig:oust_tipos_captura}
\end{figure}

Por fim, uma mudança de turno pode ainda acontecer quando um jogador não pode executar nenhum dos dois tipos de jogada. É garantido que pelo menos um jogador vai sempre poder fazer um jogada, e este é obrigado a faze-la, não podendo ocorrer a mudança de turno enquanto esta não for feita.
	

% ========================================================= %
% ============ REPRESETANÇÃO DO ESTADO DO JOGO ============ %
% ========================================================= %

\section{Representação do Estado do Jogo}
O tabuleiro é representado por uma lista de listas. A lista base terá um número de listas igual à altura do tabuleiro e cada uma dessas listas terá uma quantidade de elementos igual à largura do tabuleiro.
Esses elementos podem assumir três valores diferentes diferentes, em que cada um desses valores representa um dos três possíveis estados de uma posição do tabuleiro:

\begin{itemize}
\item \textit{a} - representa uma posição ocupada por uma peça preta.
\item \textit{b} - representa uma posição ocupada por uma peça branca.
\item \textit{x} - representa uma posição vazia.
\end{itemize}

Nas listagens \ref{lst:board-init-state}, \ref{lst:board-intermediate-state} e \ref{lst:board-final-state} são apresentados os estados fundamentais do tabuleiro e suas respetivas representações gráficas.

\begin{lstlisting}[language=Prolog, label={lst:board-init-state}, caption={Estrutura de dados para um tabuleiro vazio e sua representação.}]
[[x,x,x,x,x],[x,x,x,x,x],[x,x,x,x,x],[x,x,x,x,x],[x,x,x,x,x]]

                    5 *---*---*---*---*
                      |   |   |   |   |
                    4 *---*---*---*---*
                      |   |   |   |   |
                    3 *---*---*---*---* 
                      |   |   |   |   |
                    2 *---*---*---*---* 
                      |   |   |   |   |
                    1 *---*---*---*---*
                      A   B   C   D   E
\end{lstlisting}
\begin{lstlisting}[language=Prolog, label={lst:board-intermediate-state},caption={Estrutura de dados para um estado intermédio e sua representação.}]
[[x,b,x,x,x],[x,x,b,x,x],[x,a,a,x,x],[x,x,x,b,x],[x,x,b,b,b]]

                     5 *---B---*---*---*
                       |   |   |   |   |
                     4 *---*---B---*---*
                       |   |   |   |   |
                     3 *---A---A---*---* 
                       |   |   |   |   |
                     2 *---*---*---B---* 
                       |   |   |   |   |
                     1 *---*---B---B---B
                       A   B   C   D   E
\end{lstlisting}
\begin{lstlisting}[language=Prolog, label={lst:board-final-state},caption={Estrutura de dados para um estado final e sua representação.}]
[[x,x,x,x,x],[x,x,a,x,x],[x,a,a,x,x],[x,x,x,a,x],[x,x,a,a,a]]

                     5 *---*---*---*---*
                       |   |   |   |   |
                     4 *---*---A---*---*
                       |   |   |   |   |
                     3 *---A---A---*---* 
                       |   |   |   |   |
                     2 *---*---*---A---* 
                       |   |   |   |   |
                     1 *---*---A---A---A
                       A   B   C   D   E
\end{lstlisting}


% =================================================== %
% ============ VISUALIZAÇÃO DO TABULEIRO ============ %
% =================================================== %

\section{Visualização do Tabuleiro}
Existem seis predicados que são utilizados para a visualização do tabuleiro em modo de texto. O principal é o \textit{print\_tab(+Board)} e recebe como argumento uma lista de listas que representa o estado do tabuleiro. Os predicados restantes são meramente auxiliares e resolvem problemas particulares na visualização do tabuleiro.

\begin{itemize}
\item \textit{tab\_map(+Symb)}: converte um símbolo da estrutura de dados para um carácter e imprime-o.
\item \textit{print\_line(+Line)}: imprime uma linha do tabuleiro.
\item \textit{print\_empty\_line(+Length)}: imprime um espaço vazio entre as linhas do tabuleiro.
\item \textit{print\_column\_index(+ASCIICode,+Index)}: imprime o índice das colunas.
\item \textit{print\_tab\_aux(+Board,?LineI,?ColumnI)}: coordena a utilização dos predicados anteriores na visualização global.
\end{itemize}

De seguida um exemplo de uma evocação ao predicado \textit{print\_tab/1} e do resultado visual. Para poupar algum espaço é utilizado um tabuleiro de pequenas dimensões.

\begin{lstlisting}[language=Prolog, captionpos=b, frame=single, label={lst:print-tab-exemplo}, caption={Evocando o predicado print\_tab/1}]
print_tab([[x,x,x,x],[x,x,x,x],[b,b,a,x],[x,x,x,x]]).
\end{lstlisting}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.8]{FIGS/print-tab-exemplo-1.png}
\label{fig:print-tab-exemplo-1}
\caption{Resultado da evocação do predicado da listagem \ref{lst:print-tab-exemplo} no interpretador \textit{SICStus}.}
\end{center}
\end{figure}


% ==================================== %
% ============ MOVIMENTOS ============ %
% ==================================== %

\section{Movimentos}
Tal como descrito na secção \ref{sec:jogadas-e-turnos} existem dois tipos de movimentos (jogadas): \textit{Não-Captura} e \textit{Captura}.
Estes serão unificados sobre um único predicado que tentará adicionar um nova peça ao tabuleiro.

\begin{lstlisting}[language=Prolog, caption={Protótipo do predicado para adicionar uma peça}]
add_piece(+Piece, +Xcoord, +Ycoord, +CurrentBoard, -FinalBoard).
\end{lstlisting}

Este predicado auxiliar-se-à de predicados subsequentes para perceber a validade da adição e o tipo de jogada.


% ===================================================================== %
% ============ CONCLUSÕES E PRESPETIVAS DE DESENVOLVIMENTO ============ %
% ===================================================================== %

\section{Conclusões e Perspetivas de Desenvolvimento}
O \textit{Oust} é um jogo com poucas regras e estas são simples de entender, porém complicadas de aplicar sob o ponto de vista de um jogador recente. Os autores acreditam portanto que uma versão para computador poderá tornar o jogo mais interessante e atrativo, pois a complexidade da verificação das jogadas desaparece. Apontamos ainda a excelente qualidade do documento com as regras do \textit{Oust} que o autor mantém em \cite{oust-regras}.

A implementação da lógica do jogo será feita segundo uma abordagem \textit{bottom-up} onde os predicados mais básicos serão implementados e testados primeiro. Dada a complexidade das verificações da validade de uma jogada e do seu resultado estima-se que a implementação atual represente apenas 10\% daquilo que será a implementação final.


% ===================================== %
% ============ BIBLOGRAFIA ============ %
% ===================================== %

\clearpage
\addcontentsline{toc}{section}{Bibliografia}
\renewcommand\refname{Bibliografia}
\bibliographystyle{plain}
\bibliography{refs}


% ============================================== %
% ============ ANEXO - CÓDIGO FONTE ============ %
% ============================================== %

\newpage
\appendix
\section{Código Fonte}
\lstinputlisting[language=Prolog,numbers=left,label={lst:source-code},caption={}]
    {../SRC/oust.pl}

\end{document}
