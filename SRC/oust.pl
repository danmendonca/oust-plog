
start:-
    write('OUST'), nl,
    write('Insert the size of the board you wish to play followed by "."'), nl,
    read(Size), nl,
    newGame(Size).

/*
getCoord(L, C):-
         write('Insert the line number:'), nl,
         get_char(L),
         get_char(_),
         (L>57 -> getCoord(L, C), !; 
         write('Insert the column number:'), nl,
         get_char(C),
         get_char(_).
*/




tab_map(a):- write('A').
tab_map(b):- write('B').
tab_map(x):- write('*').
tab_map(_):- write('?').

print_line([X|[]]):- tab_map(X).
print_line([X|Xs]):- tab_map(X), write('---'), print_line(Xs).

print_empty_line(1):-
      write('|').
print_empty_line(Len):-
      Len > 1,    
      write('|   '),     
      LenNext is Len - 1,
      print_empty_line(LenNext).

print_column_index_numeric(C,C):- write(C), nl.
print_column_index_numeric(C,Ci):-
        Ci=< C,
        write(Ci), write('   '),
        Ci1 is Ci+1,
        print_column_index_numeric(C, Ci1).

aux_print_tab([X|[]], L, L, C, Ci):-
  write(' '), write(L), write(' '),
  print_line(X), nl,
  write('   '),  
  print_column_index_numeric(C, Ci).

aux_print_tab([X|Xs], Li, L, Ci, C):-
      L > 9,    
      write(L), 
      write(' '),      
      print_line(X), nl,      
      write('   '),
      print_empty_line(Ci), nl,      
      LiNext is L + 1,
      aux_print_tab(Xs, Li, LiNext, Ci, C).
    

aux_print_tab([X|Xs], Li, L, Ci, C):-
      L =< 9,    
      write(' '), 
      write(L),
      L1 is L +1,
      write(' '),      
      print_line(X), nl,      
      write('   '),
      print_empty_line(Ci), nl,      
      aux_print_tab(Xs, Li, L1, Ci, C).


print_tab([X|Xs]):-
      length([X|Xs],Vi),
      length(X,Hi),
      aux_print_tab([X|Xs], Vi, 1, Hi, 1).
	  
	  


newGame(X):-
        (X=<9 ->  
        buildLin(X, L),
        fillTab(X, L, B),
        play(B, a), !;
        nl, write('Max board size is 9, please re-enter the size:'), nl,
        read(X1), nl, newGame(X1)).

play(Board, Player):-
        print_tab(Board), nl,
        write( ' it is '), write(Player), write(' to place a move'), nl,
        write('insert the Line number followoed by "."'),
        nl,
        read(Line),
        nl,
        write('insert the Col number followed by "."'), nl,
        read(Column), nl,
        move(Line, Column, Board, Player).

endOfGame(Board, Player):-
        nl,nl,
        write('Congrats to player '), write(Player), write('!'), nl,
        write('You won the game!'), nl,
        print_tab(Board).
        



buildLin(X, L):- 
        Xi is 0,
        buildLin_(X, Xi, [], L).
        
        
buildLin_(X, X, L, L).
buildLin_(X, Xi, Progress, L):-
                append(Progress, ['x'], Nprogress),
                Xi1 is Xi+1,
                buildLin_(X,Xi1,Nprogress, L).
             

fillTab(X, L, B):-
        Xi is 0,
        fillTab_(X, Xi, L, B).
        

fillTab_(X, X, B, B).
fillTab_(X, Xi, [L| Ls], B):-
        Xi==0,
        append([[L | Ls]], [[L | Ls]], L2),
        Xi1 is Xi+2,
        fillTab_(X, Xi1, L2, B);
        
        append([L |Ls], [L], L2),
        Xi1 is Xi+1,
        fillTab_(X, Xi1, L2, B).




placement(Line, Column, Board, NewBoard, Piece):-
        placement_(Line, 1, Column, Board,[], NewBoard, Piece), !.

placement_(_, _, _, [], NewBoard, NewBoard, _).
placement_(Line, Lindex, Column, [H | T], Progress, NewBoard, Piece):-
        Line==Lindex,
        Li1 is Lindex+1,
        doPlacement(Column, H, NewL, Piece),
        append(Progress, [NewL], Nprogress),
        placement_(Line, Li1, Column, T, Nprogress, NewBoard, Piece);
        Li1 is Lindex+1,
        append(Progress, [H], Nprogress),
         placement_(Line, Li1, Column, T, Nprogress, NewBoard, Piece).



doPlacement(Cplacement, Line, NewV, Piece):-
        doPlacement_(Cplacement, 1, [], Line, NewV, Piece).


doPlacement_(_, _, NewL, [], NewL, _).
doPlacement_(Cplacement, Cindex, Progress, [H | T], NewV, Piece):-
        Cplacement==Cindex,
        Ci1 is Cindex+1,
        append(Progress, [Piece], Nprogress),
        doPlacement_(Cplacement, Ci1, Nprogress, T, NewV, Piece);
        Ci1 is Cindex+1,
        append(Progress, [H], Nprogress),
        doPlacement_(Cplacement, Ci1, Nprogress, T, NewV, Piece).
             




getPiece(Lin, Col, Board, Piece):-
        getPieceLine(Lin, 1, Col, Board, Piece),!.

getPieceLine(Lin, Lin, Col, [L|_], Piece):-
        getPiece_(Col, 1, L, Piece),!.

getPieceLine(Lin, Lini, Col, [_|Ls], Piece):-
                Lini1 is Lini + 1,
                getPieceLine(Lin, Lini1, Col, Ls, Piece),!.

getPiece_(Col, Col, [P | _], Piece):-
        P == Piece, !.

getPiece_(Col, Coli, [_| Ps], Piece):-
        Coli1 is Coli+1,
        getPiece_(Col, Coli1, Ps, Piece), !.


countGroup(Lin, Col, Board, Piece, Count):-
        length(Board, Size),
        countGr(Lin, Col, Size, Board, _, Piece, Count).

countGr(_, _, _, Fb, Fb, x, 0).
countGr(0, _, _, Fb, Fb, _, 0).
countGr(_, 0, _, Fb, Fb, _, 0).
countGr(Lin, Col, Size, Board, Nb, Piece, Count):-
        (Lin =< Size ->
        (Col =< Size ->
        (getPiece(Lin, Col, Board, Piece) ->
                placement(Lin, Col, Board, NewBoard, x),
                PLin is Lin-1, NLin is Lin+1, PCol is Col-1, NCol is Col+1,
                countGr(PLin, Col, Size, NewBoard, Nb1, Piece, C1),
                countGr(NLin, Col, Size, Nb1, Nb2, Piece, C2),
                countGr(Lin, PCol, Size, Nb2, Nb3, Piece, C3),
                countGr(Lin, NCol, Size, Nb3, Nb, Piece, C4),     
                Count is 1+ C1 + C2 +C3 +C4,!;
        countGr(Lin, Col, Size, Board, Nb, x, Count)); countGr(Lin, Col, Size, Board, Nb, x, Count));
         countGr(Lin, Col, Size, Board, Nb, x, Count)). 


afterCapture(Board, Player):-
        Player ==a,
        afterCapture_(Board, b, 1);
        afterCapture_(Board, Player,1).

afterCapture_([], _, 0).
afterCapture_(_, 1, 1).
afterCapture_([L | Ls], Player, Result):-
        member_check(Player, L),
        afterCapture_(L, 1, Result);
        afterCapture_(Ls, Player, Result).

     
member_check(X, [X|_]).
member_check(X, [Y|Ys]):- X\=Y, member_check(X, Ys).


maxSize([X|Xs], C):-
        maxSize_(X, Xs, C).

maxSize_(X, [], X).

maxSize_(X, [Y|Ys], C):-
        (X > Y ->
         maxSize_(X, Ys, C);
         maxSize_(Y, Ys, C)).


maxOppGrSize(Lin, Col, Board, ResBoard, Player, C):-
        changePlayer(Player, Piece),
         PLin is Lin-1, NLin is Lin+1, PCol is Col-1, NCol is Col+1,
         length(Board, Size),
         countGr(PLin, Col, Size, Board, Nb1, Piece, C1), append([], [C1], Size1),
         countGr(NLin, Col, Size, Nb1, Nb2, Piece, C2), append(Size1, [C2], Size2),
         countGr(Lin, PCol, Size, Nb2, Nb3, Piece, C3), append(Size2, [C3], Size3),
         countGr(Lin, NCol, Size, Nb3, Nb4, Piece, C4), append(Size3, [C4], Size4),
         maxSize(Size4, C),
         ResBoard = Nb4.
         
         
changePlayer(a, b).
changePlayer(b, a).

move(Lin, Col, Board, Player):-
        %       if is capture & if theres another move available, then  keep playing,
        %       if not, the other player takes control and does a move. If a move is available, a player must take it.
        (getPiece(Lin, Col, Board, x) ->
                placement(Lin, Col, Board, Nb1, Player),
                countGroup(Lin, Col, Nb1, Player, OwnSize),
                %OwnSize > 1, it may be a capture move
                (OwnSize > 1 ->         length(Nb1, Size),
                                        countOppGroup(Lin, Col, Size, Nb1, Player, ResBoard, OppGrSize),
                                        mergeBoards_(Player, Nb1, ResBoard, ResBoard2),
                                        (OwnSize > OppGrSize ->                                      
                                                (OppGrSize > 0 -> 
                                                 changePlayer(Player, Player2),          
                                                 (afterCapture(ResBoard2, Player2)->
                                                        (availableNonCapture(Player, ResBoard2) -> play(ResBoard2, Player);
                                                                (availableCapture(Player, ResBoard2)->play(ResBoard2, Player);
                                                                        play(ResBoard2, Player2))
                                                        ) 
                                                 ; endOfGame(Board, Player)),
                                                 !; 
                                                 nl, write('Invalid Move, try again'),nl, play(Board, Player), !);
                                        nl, write('Invalid Move, try again'),nl, play(Board, Player), ! 
                                 )
                                ; changePlayer(Player, NewPlayer),
                                  play(Nb1, NewPlayer),!);
                nl, write('Invalid Move, try again'),nl, play(Board, Player), !).


countOppGr(Lin, Col, Board, Player, Copp):-
        length(Board, Size),
        countOppGroup(Lin, Col, Size, Board, Player, _, Copp).

countOppGroup(_, _, _, Fb, x, Fb, 0).
countOppGroup(0, _, _, Fb, _, Fb, 0).
countOppGroup(_, 0, _, Fb, _, Fb, 0).
countOppGroup(Lin, Col, Size, Board, Piece, Nb, Copp):-
                (Lin =< Size ->
                (Col =< Size ->
                (getPiece(Lin, Col, Board, Piece) ->
                maxOppGrSize(Lin, Col, Board, ResBoard, Piece, OppSize),
                placement(Lin, Col, ResBoard, Nb1, x),
                PLin is Lin-1, NLin is Lin+1, PCol is Col-1, NCol is Col+1,
                countOppGroup(PLin, Col, Size, Nb1, Piece, Nb2, C1), 
                append(  [] , [C1], Size1),
                countOppGroup(NLin, Col, Size, Nb2, Piece, Nb3, C2), 
                append(Size1, [C2], Size2),
                countOppGroup(Lin, PCol, Size, Nb3, Piece, Nb4, C3), 
                append(Size2, [C3], Size3),
                countOppGroup(Lin, NCol, Size, Nb4, Piece, Nb , C4), 
                append(Size3, [C4], Size4),
                append(Size4, [OppSize], SizeF),
                maxSize(SizeF, Copp),!;
        countOppGroup(Lin, Col, Size, Board, x, Nb, Copp)); countOppGroup(Lin, Col, Size, Board, x, Nb, Copp));
         countOppGroup(Lin, Col, Size, Board, x, Nb, Copp)).

mergeBoards_(Player, Board1, Board2, Nb):-
        mergeBoards(Player, Board1, Board2, [], Nb).

mergeBoards(_, [], [], Nb, Nb).
mergeBoards(Player, [X | Xs], [Y | Ys], Temp, Nb):-
        mergeLines(Player, X, Y, [], Temp1),
        append(Temp, [Temp1], Temp2),
        mergeBoards(Player, Xs, Ys, Temp2, Nb).

mergeLines(_, [], [], Nl, Nl).
mergeLines(Player, [Player|Xs], [_|Ys], TmpLine, Temp):-
        append(TmpLine, [Player], TmpLine1),
        mergeLines(Player, Xs, Ys, TmpLine1, Temp).
mergeLines(Player, [_|Xs], [Y|Ys], TmpLine, Temp):-
        append(TmpLine, [Y], TmpLine1),
        mergeLines(Player, Xs, Ys, TmpLine1, Temp).


availableNonCapture(Player, Board):-
        length(Board, Size),
        availableNCM(1, 1, Size, Board, Player, 1).

availableNCM(Max, Max, Max, _, _, 0).

availableNCM(Lin, Col, Size,  Board, Player, C):-
        PLin is Lin-1, NLin is Lin+1, PCol is Col-1, NCol is Col+1,
        (Lin =< Size ->
        (Col =< Size ->
        isNCM(Lin, Col, Board, Player, C0),
        isNCM(PLin, Col, Board, Player, C1),
        isNCM(NLin, Col, Board, Player, C2),
        isNCM(Lin, PCol, Board, Player, C3),
        isNCM(Lin, NCol, Board, Player, C4),
        C5 is C0+C1+C2+C3+C4,
        (C5 >4 -> C is 1, !;
         availableNCM(Lin, NCol, Size, Board, Player, C)), !;
         availableNCM(NLin, 1, Size, Board, Player, C)),!; C is 0, !). 

        
isNCM(Lin, Col,Board, Player, C):-
                (getPiece(Lin, Col, Board, Player) ->
                        C is 0;
                 C is 1).

availableCapture(Player, Board):-
        length(Board, Size),
        availableCM(1,1,Size, Board, Player, 1).

availableCM(Size, Size, Size, _, _, 0).
availableCM(Lin, Col, Size, Board, Player, C):-
        PLin is Lin-1, NLin is Lin+1, PCol is Col-1, NCol is Col+1,
        (Lin=< Size->
                (Col=<Size ->
                        (getPiece(Lin, Col, Board, x)->  %uma possibilidade de meter peca
                                isNCM(PLin, Col, Board, Player, C1),
                                isNCM(NLin, Col, Board, Player, C2),
                                isNCM(Lin, PCol, Board, Player, C3),
                                isNCM(Lin, NCol, Board, Player, C4),
                                C5 is C1+C2+C3+C4,
                                (C5<4 -> %forma novo grupo
                                        placement(Lin, Col, Board, Nb1, Player),
                                        countGroup(Lin, Col, Nb1, Player, OwnSize),
                                        length(Nb1, Size),       
                                        countOppGroup(Lin, Col, Size, Nb1, Player, _, OppGrSize),
                                        (OwnSize > OppGrSize -> !,                                      
                                                (OppGrSize > 0 -> C is 1, !; 
                                                 availableCM(Lin, NCol, Size, Board, Player, C), !);
                                        availableCM(Lin, NCol, Size, Board, Player, C), !);
                                 availableCM(Lin, NCol, Size, Board, Player, C), !
                                ); 
                        availableCM(Lin, NCol, Size, Board, Player, C), !)
                 ;
                availableCM(NLin, 1, Size, Board, Player, C), !)
        ; 
        C is 0, !).

%%[PRED]
%%[ARGS]
%%[DESC]